/*
    The custom event name: packet

    Request in JSON format:
    { sender: who, action: "join" }
    { sender: who, action: "broadcast", msg: what }
    { sender: who, action: "list" }
    { sender: who, action: "quit" }

    { sender: who, action: "send", dest: whom, msg: what }
    { sender: who, action: "cgroup", group: name }
    { sender: who, action: "jgroup", group: name }
    { sender: who, action: "gbroadcast", group: name, msg: what }
    { sender: who, action: "members", group: name }
    { sender: who, action: "msgs", group: name }
    { sender: who, action: "umsgs", user: name }
    { sender: who, action: "groups"}
    { sender: who, action: "leave", group: name }
*/

const io = require('socket.io-client'); /* Load 'socket.io-client' module */
const socket = io('http://localhost:3000'); /* Connect to the server */
const readline = require('readline'); /* Load built-in 'readline' module */
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var nickname = null;

/* Take a look at https://regexr.com/ for testing */
var pattern = /^s;([A-Z0-9]+);(.+)/i;
var pattern_2 = /^bg;([A-Z0-9]+);(.+)/i; // for group broadcast

/* Listen to the 'connect' event that fired upon a successfull connection */
socket.on('connect', () => {
    /* All command-line arguments received by the shell are given to the
    process in an array called argv (short for 'argument values')
    Node.js exposes this array for every running process in the form of
    process.argv */
    nickname = process.argv[2];
    console.log("[INFO]: Welcome %s", nickname);
    socket.emit('packet', {"sender": nickname, "action": "join"});
});

/* Listen to the 'disconnect' event that fired upon a successfull disconnection */
socket.on('disconnect', (reason) => {
    console.log("[INFO]: Server disconnected, reason: %s", reason);

    /* Close the rl instance. When called, the 'close' event will be emitted */
    rl.close();
});

/* The 'line' event is emitted whenever the input stream receives
an end-of-line input (\n, \r, or \r\n). This usually occurs when
the user presses the <Enter>, or <Return> keys */
rl.on('line', (input) => {
    /* The handler function is called with a string containing
    the single line of received input */

    /* Broadcast a message: b;hello */
    if (true === input.startsWith("b;")) {
        var str = input.slice(2);
        socket.emit('packet', {"sender": nickname, "action": "broadcast",
                                "msg": str});
    }

    /* List all nicknames in the chat: ls; */
    else if ("ls;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "list"});
    }

    /* Quit the chat: q; */
    else if ("q;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "quit"});
    }

    /* Send a message secretly to someone: s;ted;hello ted */
    else if (true === pattern.test(input)) {
        var info = input.match(pattern);
        socket.emit('packet', {"sender": nickname, "action": "send",
                                "dest": info[1], "msg": info[2]});
    }

    /* Create a group: cg;friends */
    else if (true === input.startsWith("cg;")) {
        var str = input.slice(3);
        socket.emit('packet', {"sender": nickname, "action": "cgroup",
                                "group": str});
    }

    /* Join a group: j;friends */
    else if (true === input.startsWith("j;")) {
        var str = input.slice(2);
        socket.emit('packet', {"sender": nickname, "action": "jgroup",
                                "group": str});
    }

    /* Broadcast a message to a group: bg;friends;hello */
    else if (true === pattern_2.test(input)) {
        var info = input.match(pattern_2);
        socket.emit('packet', {"sender": nickname, "action": "gbroadcast",
                                "group": info[1], "msg": info[2]});
    }

    /* List all clients that are inside a group: members;friends */
    else if (true === input.startsWith("members;")) {
        var str = input.slice(8);
        socket.emit('packet', {"sender": nickname, "action": "members",
                                "group": str});
    }

    /* List the history of messages exchanged in a group:
    messages;friends */
    else if (true === input.startsWith("messages;")) {
        var str = input.slice(9);
        socket.emit('packet', {"sender": nickname, "action": "msgs",
                                "group": str});
    }

    /* List the history of messages belonging to a user:
    umessages;ted */
    else if (true === input.startsWith("umessages;")) {
        var str = input.slice(10);
        socket.emit('packet', {"sender": nickname, "action": "umsgs",
                                "user": str});
    }

    /* List the existing groups: groups; */
    else if ("groups;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "groups"});
    }

    /* Leave a group: leave;friends */
    else if (true === input.startsWith("leave;")) {
        var str = input.slice(6);
        socket.emit('packet', {"sender": nickname, "action": "leave",
                                "group": str});
    }

    /* For debugging: kkk; */
    else if ("kkk;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "kkk"});
    }

    else {
        console.log("[ERROR]: don't support this kind of syntax");
    }
});

/* Listen to the 'packet' event that is emitted from the server */
socket.on('packet', (packet_data) => {
    switch (packet_data.action) {
        case "join":
            console.log("[INFO]: %s has joined the chat", packet_data.sender);
        break;

        case "broadcast":
            console.log("%s", packet_data.msg);
        break;

        case "list":
            console.log("[INFO]: List of nicknames:");
            for (var i = 0; i < packet_data.users.length; i++) {
                console.log(packet_data.users[i]);
            }
        break;

        case "quit":
            console.log("[INFO]: %s quit the chat", packet_data.sender);
        break;

        case "send":
            console.log("%s", packet_data.msg);
        break;

        case "cgroup":
            // console.log("need?");
        break;

        case "jgroup":
            console.log("[INFO]: %s has joined the group", packet_data.sender);
        break;

        case "gbroadcast":
            console.log("%s", packet_data.msg);
        break;

        case "members":
            console.log("[INFO]: List of members:");
            for (var i = 0; i < packet_data.members.length; i++) {
                console.log(packet_data.members[i]);
            }
        break;

        case "msgs":
            console.log("[INFO]: History of messages in group", packet_data.group);
            for (var i = 0; i < packet_data.msgs.length; i++) {
                console.log(packet_data.msgs[i]);
            }
        break;

        case "umsgs":
            console.log("[INFO]: History of messages belong to", packet_data.user);
            for (var i = 0; i < packet_data.msgs.length; i++) {
                console.log(packet_data.msgs[i]);
            }
        break;

        case "groups":
            console.log("[INFO]: List of groups:");
            for (var i = 0; i < packet_data.groups.length; i++) {
                console.log(packet_data.groups[i]);
            }
        break;

        case "leave":
            console.log("[INFO]: %s left the group", packet_data.sender);
        break;

        default:
        break;
    }
});