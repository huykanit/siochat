# Socket.IO Chat

## Setup the environment on Windows
*  Install Node.js  
https://nodejs.org/en/download/

*  Install GIT for the Linux environment on Windows  
https://git-scm.com/

*  Markdown viewer
https://dillinger.io/

## Initialize the project
*  Create the `siochat` folder
*  In `siochat` folder, open `Git Bash` commnand line and create a `package.json` file as follows:
```
npm init
```
*  Then install `socket.io` module using npm as follows:
```
npm install --save socket.io
```

## Elasticsearch on Windows
### Deployment
*  Download: https://www.elastic.co/downloads/elasticsearch
*  In `elasticsearch-xxx` folder, open `Git Bash` commnand line and start Elasticsearch server as follows:
```
bin/elasticsearch.bat
```

*  Then check if the server is running as the following URL:  
http://localhost:9200/

### Testing
*  Use Postman for elasticsearch testing  
https://www.postman.com/

*  Example
```
curl --location --request GET 'http://localhost:9200/_all/_search' \
--header 'Content-Type: application/json' \
--data-raw '{
    "query" : {
        "match_all" : {}
    }
}'
```

*  Follow the `elasticsearch-xxx/README.asciidoc` file for server testing

### Developing
*  Reference:  
https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/index.html

*  In `siochat` folder, open `Git Bash` commnand line and install `elasticsearch` module using npm as follows:
```
npm install @elastic/elasticsearch
```

### Troubleshooting
Open Resource Monitor as follows:
```
resmon.exe
```

## Mocha testing

Reference:
*  https://blog.logrocket.com/a-quick-and-complete-guide-to-mocha-testing-d0e0ea09f09d/
*  https://github.com/agconti/socket.io.tests
*  https://stackoverflow.com/questions/15509231/unit-testing-node-js-and-websockets-socket-io
*  https://github.com/Swizec/random-coding/tree/master/socket.io-testing
*  https://github.com/PatMan10/testing_socketIO_server_v2

### Getting started with Mocha

Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser.  

Install Mocha as a development dependency for your project using npm as follows:
```
npm install --save-dev mocha
```

Then you can access the `mocha` binary from the `node_modules` directory of your project as follows:
```
./node_modules/mocha/bin/mocha
```

Mocha automatically looks for your tests inside the `test` directory of your project. Hence, you should go ahead and create this directory at the root in your project
```
mkdir test
```

Next, modify the `test` field in your `package.json` to run tests using Mocha. Here is what it should look like:
```
/* package.json */

{
  ...
  "scripts": {
    "test": "mocha"
  },
  ...
}
```

With this setup, you can simply run the tests in your project using this simple command:
```
npm test
```

### Writing tests
At the moment, you have everything set up for running your tests with Mocha, but you don't have any tests to run yet. The next step is to write tests for the desired functionalities of your software.

**Assertion library**
Writing tests often requires using **assertion library**. Mocha does not discriminate whatever assertion library you choose to use. In the Node.js environment, you can use the built-in **assert** module as your assertion library. However, there are more extensive assertion libraries you can use, such as **Chai**, **Expect.js**, etc.

For all the tests in this guide, **Chai** will be used as the assertion library of choice. Go ahead and install Chai as a dependency for your project as follows:
```
npm install --save-dev chai
```

You can learn more about the assertions and assertion styles Chai provides from [this documentation](https://www.chaijs.com/api/)

**Mocha interfaces**
Mocha provides a variety of interfaces for defining test suites, hooks and individual tests, namely: **BDD**, **TDD**, **Exports**, etc.

For this guide, the **BDD** style interface will be used for defining and writing the tests. You can learn more about the available style interfaces in [this documentation](https://mochajs.org/#interfaces)





